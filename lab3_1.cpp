#include<iostream> 


using namespace std;

void Max_Heapify(int a[], int i, int n) {
    int max = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;
    if ((left < n) && (a[max] < a[left])) {
        max = left;
    }
    if ((right < n) && (a[max] < a[right])) {
        max = right;
    }
    if (max != i) {
        swap(a[i], a[max]);
        Max_Heapify(a, max, n);
    }

}
void HeapSort(int a[], int n) {
    for (int i = n / 2 - 1; i >= 0; i--) {
        Max_Heapify(a, i, n);
    }
    for (int i = n - 1; i >= 0; i--) {
        swap(a[0], a[i]);
        Max_Heapify(a, 0, i);
    }

}


int main() {
    int a[100000];
    int n;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }
    HeapSort(a, n);

    for (int i = 0; i < n; i++) {
        cout << a[i] << " ";
    }
    return 0;
}